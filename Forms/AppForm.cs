﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Product_Viewer.Models;
using Product_Viewer.UserControls;

namespace Product_Viewer.Forms
{
    public partial class AppForm : Form
    {
       private ProductService ProductService { get; set; }
       private Basket Basket { get; set; }
       public AppForm(ProductService productService, Basket basket)
        {
            InitializeComponent();

            ProductService = productService;
            Basket = basket;
        }

        private void tvProducts_AfterSelect(object sender, TreeViewEventArgs e)
        {
            if (e.Node.Parent is null)
            {
                productView.Enabled = false;
                productView.Visible = false;
                return;
            }

            UpdateProductView(ProductService.Products.Where(p => p.Name == e.Node.Text).FirstOrDefault());
        }

        public void UpdateProductView(Product product)
        {
            productView.Enabled = true;
            productView.Visible = true;

            productView.Update(product);
            
        }

        private void productView_ProductSelected(object sender, ProductSelectedEventArgs e)
        {
            if (e.product is null)
                return;

            BasketItem newBasketItem = new DiscountBasketItem(new BasketItem(e.product, e.product.Price, e.Count));
            
            Basket.Add(newBasketItem);

            UpdateTotalCost();
            UpdateBasketItemView();
        }

        private void X_ProductRemoved(object sender, BasketItemRemovedEventArgs e)
        {
            

            var x =(BasketItemControl)sender;
            Basket.Remove(e.basketItem);
            x.Dispose();

            UpdateTotalCost();
        }


        private void AppForm_Load(object sender, EventArgs e)
        {
            UpdateProductTreeView();
        }

        private void UpdateProductTreeView()
        {
            foreach(var item in ProductService.Products)
            {
                string categoryName = item.Category.ToString();

                var categoryNode = tvProducts.Nodes.Find(categoryName, false);

                if (categoryNode.Length < 1)
                    tvProducts.Nodes.Add(categoryName, categoryName);
                tvProducts.Nodes[categoryName].Nodes.Add(item.Name);
            }
        }

        private void UpdateTotalCost()
        {
            lblTotalCost.Text = "Koszt: " + Basket.BasketTotalValue.ToString() + " PLN";
        }

        private void UpdateBasketItemView()
        {
            flpBasketList.Controls.Clear();

            foreach (var item in Basket.BasketItems)
            {
                var c = new BasketItemControl(item);
                c.BasketItemRemoved += X_ProductRemoved;
                flpBasketList.Controls.Add(c);
            }
        }
    }
}
