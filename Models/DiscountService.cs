﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class DiscountService
    {
        public IReadOnlyList<DiscountPerProduct> DiscountPerProducts => new[]
        {
            new DiscountPerProduct("Silicone Baking Mat", 2, 0.1),
            new DiscountPerProduct("Mind & Body Candle", 3, 0.15),
        };

        public IReadOnlyCollection<DiscountPerBasket> DiscountPerBaskets => new[]
        {
            new DiscountPerBasket(2, 0.1)
        };
    }
}
