﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class DiscountPerBasket : Discount
    {
        public int MinimalCategoryOrder { get; set; }

        public DiscountPerBasket(int minimalCategoryOrder, double value) : base(value)
        {
            MinimalCategoryOrder = minimalCategoryOrder;
        }
    }
}
