﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    public class BasketItem
    {
        public Product Product { private set; get; }
        public virtual int Count { protected set; get; }
        public virtual decimal Price { protected set; get; }

        public decimal TotalAmount 
        { 
            get
            {
                return Decimal.Round((Price * Count), 2);
            }
        }
        public BasketItem(Product product, decimal price, int count)
        {
            Product = product;
            Count = count;
            Price = price;
        }
    }
}
