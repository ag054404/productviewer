﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class DiscountPerProduct : Discount
    {
        public String ProductName { get; set; }
        public int MinimalOrder { get; set; }

        public DiscountPerProduct(string productName, int minimalOrder, double value) : base(value) 
        {
            ProductName = productName;
            MinimalOrder = minimalOrder;
        }
    }
}
