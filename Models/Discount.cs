﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class Discount
    {
        public double Value { get; set; }

        public Discount(double value)
        {
            this.Value = value;
        }
    }
}
