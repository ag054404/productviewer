﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    public class Basket
    {
        public List<BasketItem> BasketItems { private set; get; } = new List<BasketItem>();

        public decimal BasketTotalValue 
        {
            get => UpdateBasketValue();
        }

        public void Add(BasketItem basketItem)
        {
            var existingItems = FindInBasket(basketItem);

            if (!(existingItems is null))
            {
                BasketItems.Remove(existingItems);
                basketItem = new DiscountBasketItem( new BasketItem(existingItems.Product, existingItems.Price, existingItems.Count + basketItem.Count));
            }

            BasketItems.Add(basketItem);
                
        }

        public void Remove(BasketItem basketItem)
        {
            var existingItems = FindInBasket(basketItem);
            if (!(existingItems is null))
                BasketItems.Remove(existingItems);
        }

        private decimal UpdateBasketValue()
        {
            decimal amount = 0.0m;

            foreach (var item in BasketItems)
                amount += item.TotalAmount;

            

            //if (x >= 2)
            //    amount *= 0.9m;
            ApplyDiscount(ref amount);

            return Decimal.Round(amount, 2);
        }

        private BasketItem FindInBasket(BasketItem basketItem)
        {
            return BasketItems.Find(i => i.Product.Name == basketItem.Product.Name);
        }

        private void ApplyDiscount(ref decimal amount)
        {
            Double value = FindDiscountValue();
            amount *= (decimal)(1 - value);
        }
        private double FindDiscountValue()
        {
            DiscountService discountService = new DiscountService();
            var x = BasketItems.Select(i => i.Product).Select(p => p.Category).Distinct().Count();

            Discount discount = discountService.DiscountPerBaskets
                .Where(i => i.MinimalCategoryOrder <= x)
                .FirstOrDefault();

            if (!(discount is null))
                return discount.Value;
            return 0.0;
        }
    }
}
