﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class DiscountForBasketItem : Discount
    {
        Product product;
        int minimumOrderQuantity;

        public DiscountForBasketItem(Product product, int minimumOrderQuantity, double value) : base(value)
        {
            this.product = product;
            this.minimumOrderQuantity = minimumOrderQuantity;
        }
    }
}
