﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Product_Viewer.Models
{
    class DiscountBasketItem : BasketItem
    {
        public override decimal Price => base.Price;
        public DiscountBasketItem(BasketItem basketItem) : base(basketItem.Product, basketItem.Price, basketItem.Count)
        {
            ApplyDiscount();
        }

        private void ApplyDiscount()
        {
            Double value = FindDiscountValue();
            Price *= (decimal)(1 - value);
        }
        private double FindDiscountValue()
        {
            DiscountService discountService = new DiscountService();
            Discount discount = discountService.DiscountPerProducts
                .Where(i => i.ProductName == base.Product.Name)
                .Where(i => i.MinimalOrder <= base.Count)
                .FirstOrDefault();
            
            if (!(discount is null))
                return discount.Value;
            return 0.0;
        }
    }
}
