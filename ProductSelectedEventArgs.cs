﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Product_Viewer.Models;

namespace Product_Viewer
{
    public class ProductSelectedEventArgs : EventArgs
    {
        public Product product;
        public int Count;
    }
}
