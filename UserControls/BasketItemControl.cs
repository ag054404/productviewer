﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Product_Viewer.Models;

namespace Product_Viewer.UserControls
{
    public partial class BasketItemControl : UserControl
    {
        public event EventHandler<BasketItemRemovedEventArgs> BasketItemRemoved;

        protected virtual void OnProductRemoved(BasketItemRemovedEventArgs e)
        {
            EventHandler<BasketItemRemovedEventArgs> handler = BasketItemRemoved;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        private BasketItem BasketItem { get; set; }
       
        public BasketItemControl(BasketItem basketItem)
        {
            InitializeComponent();
            BasketItem = basketItem;

            UpdateComponent();
        }

        private void UpdateComponent()
        {
            pictureBox1.Load(BasketItem.Product.IamgeUrl);
            label1.Text = BasketItem.Price + " PLN x " + BasketItem.Count;
            label2.Text = "RAZEM: " + BasketItem.TotalAmount;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            BasketItemRemovedEventArgs args = new BasketItemRemovedEventArgs();
            args.basketItem = BasketItem;
            OnProductRemoved(args);
        }
    }
}
