﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Product_Viewer.Models;

namespace Product_Viewer.UserControls
{
    public partial class ProductControl : UserControl
    {
        public event EventHandler<ProductSelectedEventArgs> ProductSelected;

        protected virtual void OnProductSelected(ProductSelectedEventArgs e)
        {
            EventHandler<ProductSelectedEventArgs> handler = ProductSelected;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        Product _product;
        
        public ProductControl()
        {
            InitializeComponent();
        }

        public void Update(Product product)
        {
            _product = product;
            lblProductName.Text = "Produkt: " + product.Name;
            lblProductCategory.Text = "Karegoria: " + product.Category.ToString();
            lblProductDescription.Text = product.Description;
            lblPrize.Text = "Cena: " + product.Price + " PLN";
            lblSource.Text = "Źródło: " + product.Source;
            SecurelyLoadProductImage(product.IamgeUrl);
            nudCounter.Value = 0m;
        }

        private void SecurelyLoadProductImage(string url)
        {
            try
            {
                pbProductImage.LoadAsync(url);
            }
            catch(Exception e)
            {
                pbProductImage.Image.Dispose();
            }
        }

        private void nudCounter_ValueChanged(object sender, EventArgs e)
        {
            btnAddToBasket.Enabled = (nudCounter.Value > 0) ? true : false;
        }

        private void btnAddToBasket_Click(object sender, EventArgs e)
        {
            ProductSelectedEventArgs args = new ProductSelectedEventArgs();
            args.product = _product;
            args.Count = (int)nudCounter.Value;
            OnProductSelected(args);
        }

       

    
    }
}
